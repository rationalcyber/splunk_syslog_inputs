##########################################################################
#  Copyright (C) 2016-2017  Rational Cyber LLC
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#  See the GNU General Public License for more details.
#
#  A copy of the GNU General Public License is provided in the LICENSE
#  file. If not, see <http://www.gnu.org/licenses/> for additional detail.
##########################################################################

# Make an inputs.conf, props.conf, and outputs.conf for syslog monitoring

import csv
import datetime
import os
import shutil
import sys
import ast
import rcutils


class SyslogMaker:

  def __init__(self,config_dict):
    self.configs=dict(config_dict)
    self.base_directory = self.configs['base_dir']
    self.syslog_input_file = os.path.join(self.base_directory,self.configs['syslog_input_file'])
    self.catchall_blacklist = []
    self.catchall_regex=""
    self.mod_strings=dict()
    self.mod_strings['props']=self.get_file_banner()
    self.mod_strings['inputs']=self.get_file_banner()
    self.mod_strings['outputs']=self.get_file_banner()
    self.catchall_index = str(self.configs['syslog_catchall_index'])
    # Format for path is essentially /mnt/$loghost/log/$date/$fromhost/$host/$facility.log
    # ...make sure host_segment is consistent with static_path
    self.static_path = str(self.configs['syslog_static_path'])
    self.host_segment = self.configs['syslog_host_segment']
    self.app_dir= os.path.join(self.base_directory,self.configs['syslog_app_dir'])
    self.boilerplate=dict()
    self.boilerplate['src_dir']=os.path.join(self.base_directory,self.configs['syslog_template_dir'])
    self.boilerplate['apps']=dict()
    self.boilerplate['apps']['props']=dict()
    self.boilerplate['apps']['props']['template']='syslog_props'
    self.boilerplate['apps']['props']['target']=self.configs['syslog_props_app']
    self.boilerplate['apps']['props']['token_string']=self.configs['syslog_props_token']
    self.boilerplate['apps']['inputs']=dict()
    self.boilerplate['apps']['inputs']['template']='syslog_inputs'
    self.boilerplate['apps']['inputs']['target']=self.configs['syslog_inputs_app']
    self.boilerplate['apps']['inputs']['token_string']=self.configs['syslog_inputs_token']
    self.boilerplate['apps']['outputs']=dict()
    self.boilerplate['apps']['outputs']['template']='syslog_outputs'
    self.boilerplate['apps']['outputs']['target']=self.configs['syslog_outputs_app']
    self.boilerplate['apps']['outputs']['token_string']=self.configs['syslog_outputs_token']
    self.copy_boilerplate_apps()
    self.routing_groups=ast.literal_eval(self.configs['syslog_tcprouting_nondefaultgroups'])
    self.default_routing_group = self.configs['syslog_tcprouting_defaultgroup']
    self.indexer_groups=self.configs['indexer_group_definition_file']
    self.outputs_sslCertPath = self.configs['syslog_outputs_sslCertPath']
    self.outputs_sslRootCAPath = self.configs['syslog_outputs_sslRootCAPath']

  def copy_boilerplate_apps(self):
    if not os.path.exists(self.app_dir):
      os.makedirs(self.app_dir)
    for conf_file,app in self.boilerplate['apps'].items():
      src_template = os.path.join(self.boilerplate['src_dir'],app['template'])
      dest_template = os.path.join(self.app_dir,app['template'])
      dest_app = os.path.join(self.app_dir,app['target'])
      if os.path.exists(dest_app):
        shutil.rmtree(dest_app)
      if os.path.exists(dest_template):
        shutil.rmtree(dest_template)
      shutil.copytree(src_template, dest_template)
      shutil.move(dest_template,dest_app)

  def get_file_banner(self):
    s="#####################################\n"
    s=s+"#                                   #\n"
    s=s+"# THIS FILE AUTOMATICALLY GENERATED #\n"
    s=s+"#                                   #\n"
    s=s+"#####################################\n"
    s=s+"\n# last build : "+str(datetime.datetime.now())+"\n\n"
    return s

  def write_element(self,name,delim,data,envelope):
    if envelope is None:
       envelope=["",""]
    s=str(envelope[0])+str(name)+str(delim)+str(data)+str(envelope[1])+"\n"
    self.mod_strings['inputs']=self.mod_strings['inputs'] + s

  def write_sourcetype(self,sourcetype):
    delim=" = "
    self.write_element("sourcetype",delim,sourcetype,None)

  def write_tcprouting(self,indexer_group):
    delim=" = "
    self.write_element("_TCP_ROUTING",delim,indexer_group,None)

  def write_blacklist(self,blacklist):
    delim=" = "
    self.write_element("blacklist",delim,blacklist,None)

  def write_whitelist(self,whitelist):
    delim=" = "
    self.write_element("whitelist",delim,whitelist,None)

  def write_disabled(self,disabled):
    delim=" = "
    self.write_element("disabled",delim,disabled,None)

  def write_index(self,index):
    delim=" = "
    self.write_element("index",delim,index,None)

  def write_monitor(self,expression):
    delim="://"
    envelope=[ "[", "]" ]
    self.mod_strings['inputs']=self.mod_strings['inputs']+"\n"
    self.write_element("monitor",delim,expression,envelope)
    self.append_catchphrase(expression)

  def convert_expression(self,expression):
    regex = expression.replace(".", "\.").replace("*", ".*")
    return regex

  def append_catchphrase(self,expression):
    regex="(" + expression[len(self.static_path):] + ")"
    regex = self.convert_expression(regex)
    self.catchall_blacklist.append(regex)

  def write_catchall(self):
    delim = "|"
    catchall_regex = delim.join(self.catchall_blacklist)
    s = "\n[monitor://"+self.static_path+"]\n"
    s=s+"host_segment = "+str(self.host_segment)+"\n"
    s=s+"index = "+self.catchall_index+"\n"
    s=s+"blacklist = " + self.convert_expression(self.static_path)+"("+str(catchall_regex)+")\n"
    s=s+"disabled = 0\n"
    self.mod_strings['inputs'] = self.mod_strings['inputs'] + s
    self.catchall_regex = str(catchall_regex)

  def update_props(self,props_string,source,tz,linemerge):
    # add TZ clarification to props
    tmp_str=""
    # if source ends with a /, then append an asterisk
    if source[-1]=="/":
      tmp_str=str(source)+"*"
    else:
      tmp_str=str(source)

    # add SHOULD_LINEMERGE clarification
    tmp_linemerge = linemerge.lower()
    if linemerge=="":
      tmp_linemerge = false

    tmp_str = "[source::"+tmp_str+"]"
    if tz!="":
      tmp_str = tmp_str + "\nTZ = " + str(tz)
    tmp_str = tmp_str + "\nSHOULD_LINEMERGE = " + tmp_linemerge
    tmp_str = tmp_str + "\n\n"
    return props_string + tmp_str

  def replace_tokens(self,filename,token_string,replacement_string):
    # Open template file and replace every occurrence of "token_string" with
    # "replacement_string"; output to same file
    lines = []
    with open(filename) as infile:
      for line in infile:
        line = line.replace(token_string, replacement_string)
        lines.append(line)
    with open(filename, 'w') as outfile:
      for line in lines:
        outfile.write(line)

  def write_props_file(self):
    props_file = os.path.join(self.app_dir,self.boilerplate['apps']['props']['target'],"local","props.conf")
    token_string=self.boilerplate['apps']['props']['token_string']
    self.replace_tokens(props_file,token_string,self.mod_strings['props'])

  def write_inputs_file(self):
    inputs_file = os.path.join(self.app_dir,self.boilerplate['apps']['inputs']['target'],"local","inputs.conf")
    token_string=self.boilerplate['apps']['inputs']['token_string']
    self.replace_tokens(inputs_file,token_string,self.mod_strings['inputs'])

  def write_outputs_file(self):
    outputs_file = os.path.join(self.app_dir,self.boilerplate['apps']['outputs']['target'],"local","outputs.conf")
    token_string=self.boilerplate['apps']['outputs']['token_string']
    self.replace_tokens(outputs_file,token_string,self.mod_strings['outputs'])

  def assign_routing(self,index_name):
    indexer_group=""
    for group in self.routing_groups:
      if str(index_name).startswith(group):
        indexer_group=self.routing_groups[group]
    if indexer_group!="":
      self.write_tcprouting(indexer_group)

  def process_syslog_csv(self,csv_f):
    ''' Syslog input csv contains several stanza-buidling columns includding:
       - index
       - sourcetype
       - case_dependent_path
       - blacklist
       - whitelist
       - timezone
       - disabled
       - should_linemerge'''

    for row in csv_f:
      if row['case_dependent_path']!="":
        self.write_monitor(row['case_dependent_path'])
        self.mod_strings['inputs'] = self.mod_strings['inputs'] + "host_segment = "+str(self.host_segment)+"\n"
        self.write_index(row['index'])
        self.assign_routing(row['index'])
        if row['sourcetype']!="":
           self.write_sourcetype(row['sourcetype'])
        if row['blacklist']!="":
           self.write_blacklist(row['blacklist'])
        if row['whitelist']!="":
           self.write_whitelist(row['whitelist'])
        if row['timezone']!="" or row['should_linemerge']!="":
           self.mod_strings['props']= self.update_props(self.mod_strings['props'],row['case_dependent_path'],row['timezone'],row['should_linemerge'])
        if str(row['timezone']).lower=="1" or str(row['timezone']).lower=="true":
           self.write_disabled(1)
        else:
           self.write_disabled(0)

  def load_indexer_groups(self):
    try:
      f=open(os.path.join(self.base_directory,self.indexer_groups),"r")
      csv_f=csv.DictReader(f)
      indexer_groups=dict()
      for row in csv_f:
        indexer_groups[row['indexer_group']] = row['server']
      f.close()
      return indexer_groups
    except:
      print("Failed to load indexer groups file.")
      sys.exit(1)

  def build_syslog_outputs(self):
    indexer_groups = self.load_indexer_groups()
    syslog_output_string = ""
    syslog_output_string = syslog_output_string +"[tcpout]\ndefaultGroup = "+self.default_routing_group+"\n\n"
    for group in indexer_groups:
      syslog_output_string = syslog_output_string + "[tcpout:"+str(group)+"]\nserver = "+indexer_groups[group]+"\n"

      # if certs are defined, then let's turn on SSL/TLS outputs
      if self.outputs_sslCertPath!="" and self.outputs_sslRootCAPath!="" :
        syslog_output_string = syslog_output_string + "useClientSSLCompression = true\nsslCertPath = "+self.outputs_sslCertPath+"\n"
        syslog_output_string = syslog_output_string + "sslRootCAPath = "+self.outputs_sslRootCAPath+"\n"
        syslog_output_string = syslog_output_string + "sslVerifyServerCert = true\nsslVersions = tls1.2\n"

      syslog_output_string = syslog_output_string + "maxQueueSize = 64MB\n\n"
    self.mod_strings['outputs']=self.mod_strings['outputs']+syslog_output_string

  def make_syslog(self):
    # Reads syslog input file and creates all output files in the target directory
    try:
      f=open(self.syslog_input_file,'r')
      csv_f = csv.DictReader(f)
      self.process_syslog_csv(csv_f)
      f.close()
    except:
      print("Couldn't process syslog inputs csv file.")
      sys.exit(1)

    self.write_catchall()
    self.build_syslog_outputs()
    self.mod_strings['props'] = self.mod_strings['props'] + "\n#### END OF PROPS.CONF ####"
    self.mod_strings['inputs'] = self.mod_strings['inputs'] + "\n#### END OF INPUTS.CONF ####"
    self.mod_strings['outputs']= self.mod_strings['outputs'] + "\n#### END OF OUTPUTS.CONF ####"
    self.write_props_file()
    self.write_inputs_file()
    self.write_outputs_file()

def load_config(filename):
  config_dict=dict()
  try:
    config_dict=dict()
    config_dict["syslog_props_token"] = "$SYSLOG_PROPS_TOKEN$"  #Tokens in template apps that will be replaced with autogenerated content
    config_dict["syslog_inputs_token"]= "$SYSLOG_INPUTS_TOKEN$"
    config_dict["syslog_outputs_token"] = "$SYSLOG_OUTPUTS_TOKEN$"
    f=open(filename,'r')
    csv_f = csv.DictReader(f)
    for row in csv_f:
      config_dict[row['variable']] = str(row['value'])
    f.close()
  except:
    print("Loading config from '"+str(filename)+"' failed.")
    sys.exit(1)
  return config_dict


if  __name__ == "__main__":

  if len(sys.argv)>1:
    config_dict=load_config(sys.argv[1])
  else:
    print("USAGE: python syslog_maker.py config_file_path")
    sys.exit(1)

  sm=SyslogMaker(config_dict)
  rcutils.print_banner(__file__)
  print("configuration used = "+ str(config_dict))
  sm.make_syslog()

  # Test to make sure catchall is not too long
  if len(sm.catchall_regex)>32795:
    print("\n\n   ERROR: catchall regex is too long !!!!! \n")
    sys.exit(1)
  else:
    percent_full = float(len(sm.catchall_regex))*100/float(32795)
    print("\n\nSyslog catchall regex is {0:.2f}% of max.\n".format(percent_full))
    sys.exit(0)
