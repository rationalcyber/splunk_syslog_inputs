#!/usr/bin/env python
#encoding: utf-8
##########################################################################
#  Copyright (C) 2016-2017  Rational Cyber LLC
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#  See the GNU General Public License for more details.
#
#  A copy of the GNU General Public License is provided in the LICENSE
#  file. If not, see <http://www.gnu.org/licenses/> for additional detail.
##########################################################################

def print_banner(program):
  print("\n"+str(program)+" Copyright (C) 2016-2017 Rational Cyber LLC")
  print("This program comes with ABSOLUTELY NO WARRANTY. This is free software,")
  print("and you are welcome to redistribute it under certain conditions; ")
  print("see <http://www.gnu.org/licenses/> for details.\n")
